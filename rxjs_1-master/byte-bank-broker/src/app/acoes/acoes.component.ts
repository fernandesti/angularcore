import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { merge, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, switchMap } from 'rxjs/operators';
import { Acoes } from './modelo/acoes';
import { AcoesService } from './service/acoes.service';

const TIME_INPUT = 300;
@Component({
  selector: 'app-acoes',
  templateUrl: './acoes.component.html',
  styleUrls: ['./acoes.component.css'],
})
export class AcoesComponent {

  acoesInput = new FormControl();

  //private inscricao: Subscription;

  constructor(private acoesService: AcoesService) {}

  todasAcoes$ = this.acoesService.getAcoes();
  acaoInput$ =  this.acoesInput.valueChanges.pipe(
    debounceTime(TIME_INPUT),
    filter(valorInput => valorInput.length >= 3 || !valorInput.lenth),
    distinctUntilChanged(),
    switchMap((input: string) => this.acoesService.getAcoes(input))
  );

  acoes$ = merge(this.todasAcoes$, this.acaoInput$);
  // ngOnInit(): void {
  //   this.inscricao = this.acoesService.getAcoes().subscribe(acao => {
  //     this.acoes = acao;
  //   });
  // }

  // ngOnDestroy(): void {
  //   this.inscricao.unsubscribe();
  // }

}
