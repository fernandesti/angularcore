import { Acoes } from "./acoes";

export interface AcaoAPI {
  payload: Acoes;
}
