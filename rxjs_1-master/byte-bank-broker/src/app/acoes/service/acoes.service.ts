import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AcaoAPI } from '../modelo/acao-api';
import { map, pluck, tap } from 'rxjs/operators';
import { Acao } from '../modelo/acao';

@Injectable({
  providedIn: 'root'
})
export class AcoesService {

  API = 'http://localhost:3000/acoes';

  constructor(private httpCliente: HttpClient) { }

  getAcoes(acao?: string){
    const params = acao ? new HttpParams().append('valor', acao) : undefined;
    return this.httpCliente.get<AcaoAPI>(this.API, { params })
    .pipe(
      //tap(data => console.log(data)),
      pluck('payload'),
      map(acoes => acoes.sort((acaoA, acaoB) => this.ordernarAcoes(acaoA, acaoB)))
    );
  }

  ordernarAcoes(acaoA: Acao, acaoB: Acao){
    if(acaoA.codigo > acaoB.codigo) return 1;
    if(acaoA.codigo < acaoB.codigo) return -1;
    return 0;
  };

}
